//Import core node modules
import readline from 'readline';
import fs from 'fs';

//Import core process and utility functions
//import process and utility functions
import {calculateMontlyIncomeTaxFor,
		aboveBaseTax,
		grossMonthlyIncomeFor,
		netIncome,
		totalSuperFor} from './processFunctions.js';
import {appendFile} from './util.js';

//import Employee class
import Employee from './employee.js';

//Node Core ReadLine Module to read a file line by line, emitting a 'line' event each time the module identifies '\n'
const rl = readline.createInterface({
    input: fs.createReadStream('./src/input.txt'),
    output: fs.appendFile('./src/output.txt'),
    terminal: false
});

//Line event triggers entire program
rl.on('line', function(line) {
	var inputData = line.split(',');
	const newEmployee = new Employee(inputData);

	_calculateIncomeTaxFor(newEmployee).then((data)=>{
		appendFile(data + "\r\n");
	}) 
    
});

//Promise based function, carrying a hash table to mutate its attributal state
export function _calculateIncomeTaxFor(newEmployee){
    const {employeeIncome} = newEmployee

    return grossMonthlyIncomeFor(newEmployee,employeeIncome)
    .then((employeeInstance)=>{
      return calculateMontlyIncomeTaxFor(employeeInstance,employeeIncome);
    })
    .then((employeeInstance)=>{
      return netIncome(employeeInstance,employeeIncome);
    })
    .then((employeeInstance)=>{
      return totalSuperFor(employeeInstance,employeeIncome);
    })
    .then((employeeInstance)=>{
      return [newEmployee.getName(),
              newEmployee.getPayPeriod(),
              employeeIncome.getGrossIncome(),
              employeeIncome.getIncomeTax(),
              employeeIncome.getNetIncome(),
              employeeIncome.getSuperTax()].join(", ");
    })

}