import path from 'path';
import {expect} from 'chai';
import sinon from 'sinon';
import 'babel-polyfill';

import {Signal} from '../Signal.js';
import {TrafficJunction} from '../TrafficJunction.js';
import {_each,_filter,_isFunction} from '../util.js';
let server = require(path.join(__dirname, '..', './server.js'));

let Context = {};
let sandbox;

describe('creating a Signal instance',()=>{
  'use strict';

  before('Create a Signal instance',(done)=>{
    Context["North"] = new Signal('North','Red');

    sandbox = sinon.sandbox.create();

    sandbox.stub(console, "log");
    done();
  })

  after('Delete the Signal instance',(done)=>{
    delete Context["North"];
    sandbox.restore();
    done();
  })
    
  it('Instance created successfully and is an object',()=>{
    expect(Context["North"]).to.be.an('object');

  });

  it('Expect instance to have property name',()=>{
    expect(Context["North"]).to.have.property('name');

  });

  it('Expect instance to have property state',()=>{
    expect(Context["North"]).to.have.property('state');

  });

  it('Expect instance state to have property Red',()=>{
    expect(Context["North"]).to.have.deep.property('state.Red',true);

  });

  it('Expect instance state to have property Green',()=>{
    expect(Context["North"]).to.have.deep.property('state.Green',false);
  });

  it('Expect instance state to have property Yellow',()=>{
    expect(Context["North"]).to.have.deep.property('state.Yellow',false);
  });

  it('Expect instance state printActiveState() to console.log correctly',()=>{
     Context["North"].printActiveState();
     expect(console.log).to.be.called;
  });

  it('Expect instance state changeActiveStateTo("Green") to change active state to Green',()=>{
     Context["North"].changeActiveStateTo("Green");
     expect(Context["North"]).to.have.deep.property('state.Green',true);
     expect(Context["North"]).to.have.deep.property('state.Red',false);
  });

  it('Expect instance state returnActiveState() to return Red',()=>{
     Context["North"].changeActiveStateTo("Red");
     expect(Context["North"].returnActiveState()).to.equal("Red");
  });

  it('Expect instance state makeGreen() to change active state to Green',()=>{
     Context["North"].makeGreen();
     expect(Context["North"]).to.have.deep.property('state.Green',true);
  });

  it('Expect instance state makeRed() to change active state to Red',()=>{
     Context["North"].makeRed();
     expect(Context["North"]).to.have.deep.property('state.Red',true);
  });

  it('Expect instance state makeYellow() to change active state to Yellow',()=>{
     Context["North"].makeYellow();
     expect(Context["North"]).to.have.deep.property('state.Yellow',true);
  });
});

// **********************************

describe('creating a Traffic Junction instance',()=>{
  'use strict';

  before('Create a Signal instance',(done)=>{
    Context["TrafficJunc"] = new TrafficJunction('Red','Red','Red','Red');

    sandbox = sinon.sandbox.create();

    // stub some console methods
    sandbox.stub(console, "log");
    done();
  })

  after('Delete the Signal instance',(done)=>{
    delete Context["TrafficJunc"];
    sandbox.restore();
    done();
  })

  it('Junction Instance created successfully and is an object',()=>{
    expect(Context["TrafficJunc"]).to.be.an('object');
  });
  
  it('Expect instance to have properties North',()=>{
    expect(Context["TrafficJunc"]["state"]).to.have.property('North');
  });

  it('Expect instance to have properties South',()=>{
    expect(Context["TrafficJunc"]["state"]).to.have.property('South');
  });

  it('Expect instance to have properties East',()=>{
    expect(Context["TrafficJunc"]["state"]).to.have.property('East');
  });

  it('Expect instance to have properties West',()=>{
    expect(Context["TrafficJunc"]["state"]).to.have.property('West');
  });

  it('Expect instance to have print status of each signal in junction',()=>{
     Context["TrafficJunc"].printCurrentState(); 
     expect(console.log).to.be.called;
  });

  it('Expect checkCurrentActiveSignalKey() to return activeState',()=>{
     expect(Context["TrafficJunc"].checkCurrentActiveSignalKey("North")).to.be.true;
  });

  it('Expect checkCurrentActiveSignalValue() to return activeState',()=>{
     expect(Context["TrafficJunc"].checkCurrentActiveSignalValue("Red")).to.be.true;
  });

  it('Expect checkCurrentActiveSignal() to return activeState',()=>{
     expect(Context["TrafficJunc"].checkCurrentActiveSignal("North","Red")).to.be.true;
  });

  it('Expect makeGreen("North") to make the North instance Green',()=>{
     Context["TrafficJunc"].makeGreen("North");
     expect(Context["TrafficJunc"].state.North.returnActiveState()).to.equal("Green");
  });

  it('Expect makeYellow("North") to make the North instance Yellow',()=>{
     Context["TrafficJunc"].makeYellow("North")
     expect(Context["TrafficJunc"].state.North.returnActiveState()).to.equal("Yellow");
  });

  it('Expect makeRed("North") to make the North instance Red',()=>{
     Context["TrafficJunc"].makeRed("North");
     expect(Context["TrafficJunc"].state.North.returnActiveState()).to.equal("Red");
  });

  it('Expect makeChange("Red","North","makeRed") to make the North instance Red',()=>{
     Context["TrafficJunc"].makeChange("Red","North","makeRed");
     expect(Context["TrafficJunc"].state.North.returnActiveState()).to.equal("Red");
     expect(Context["TrafficJunc"].currentActiveSignal.signal).to.equal("North");
     expect(Context["TrafficJunc"].currentActiveSignal.activeState).to.equal("Red");
  });


  it('Expect initial call to call printCurrentState and makeGreen with East as argument ',()=>{
    Context["TrafficJunc"].initialCall();
    let mock = sinon.mock(Context["TrafficJunc"]);
    mock.expects("initialCall").once();
    mock.expects("makeGreen").withArgs("East");
  })

  it('Expect East and West to be made yellow after 270 seconds if they are both active',()=>{
    var clock = sinon.useFakeTimers();
    Context["TrafficJunc"].makeGreen("East");
    Context["TrafficJunc"].decisionToBeMade();
    var initialState = Context["TrafficJunc"].checkCurrentActiveSignal("East","Green");
    expect(initialState).to.be.true;
    clock.tick(270001);
    var afterState = Context["TrafficJunc"].checkCurrentActiveSignal("East","Yellow");
    expect(afterState).to.be.true;
    clock.restore();
  })

})

describe('util functions',()=>{
  it("Expect using the each function to loop through an array and edit it",()=>{
    let arr = [];
    _each([1,2,3,4,5],(elem)=>{arr.push(elem)})
    expect(arr).to.have.deep.property('[0]',1);
  });

  it("Expect using the filter function to filter an array",()=>{
    let arr = _filter([1,2,3,4,5],(elem)=>{return elem === 1})
    expect(arr[0].state).to.equal(0);
  })
})
