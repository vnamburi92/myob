import employeeIncome from './employeeIncome.js';

//Employee class, variables made accessible using traditional getter and setter methods to allow for data malleability
export default class Employee {

	constructor(rawInput){
		this.firstName = rawInput[0];
		this.lastName = rawInput[1];
		this.payPeriod = rawInput[4];
		this.employeeIncome = new employeeIncome(rawInput);
	}

	getName(){
		return `${this.firstName} ${this.lastName}`;
	}

	getPayPeriod(){
		return this.payPeriod;
	}

}