
//Income class, variables made accessible using traditional getter and setter methods to allow for data malleability
export default class Income {

	constructor(inputData){
		this.annualIncome = inputData[2];
		this.superAmount = inputData[3];

		this.grossIncome = 0;
		this.incomeTax = 0;
		this.netIncome = 0;
		this.superTax = 0;
	}

	getAnnualIncome(){
		return parseInt(this.annualIncome);
	}

	getSuperAmount(){
		return parseInt(this.superAmount)/100;
	}

	getGrossIncome(){
		return this.grossIncome;
	}
	setGrossIncome(amount){
		this.grossIncome = amount;
	}

	getIncomeTax(){
		return this.incomeTax;
	}
	setIncomeTax(amount){
		this.incomeTax = amount;
	}

	getNetIncome(){
		return this.netIncome;
	}
	setNetIncome(amount){
		this.netIncome = amount;
	}

	getSuperTax(){
		return this.superTax;
	}
	setSuperTax(amount){
		this.superTax = amount;
	}

}