const exports = exports || module.exports || null;

if (exports) {
	exports.ABOVE_180000 = 0.45;
	exports.ABOVE_80000 = 0.37;
	exports.ABOVE_37000 = 0.325;
	exports.ABOVE_18200 = 0.19;
	exports.BELOW_18200 = 0;

	exports.BASE_FOR_180000 = 54547;
	exports.BASE_FOR_80000 = 17547;
	exports.BASE_FOR_37000 = 3572;
	
	exports.SUPER_PERCENTAGE = 0.09;
}
