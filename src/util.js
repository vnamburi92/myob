import fs from 'fs';

//Append data to a file/create file if it doesn't exist
export function appendFile(data){
  fs.appendFile('./src/output.txt', data);
}

//Round number to two decimal places -> 50 as median
export function roundOff(value){
  const decimalPoint = parseInt((value.toFixed(2) + "").split(".")[1]);
  if(decimalPoint >= 50){
    return Math.ceil(value);
  }
  else{
    return Math.floor(value);
  }
}