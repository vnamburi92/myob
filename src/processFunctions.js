import fs from 'fs';
import Employee from './employee.js';
import {roundOff} from './util.js';

import {ABOVE_180000,
        ABOVE_80000,
        ABOVE_37000,
        ABOVE_18200,
        BELOW_18200,
        BASE_FOR_180000,
        BASE_FOR_80000,
        BASE_FOR_37000,
        SUPER_PERCENTAGE} from './constants.js';

//Tax calculator

// If/Else based switch function for identifying tax bracket, and adding base amount with additional value
export function calculateMontlyIncomeTaxFor(employeeInstance,employeeIncome){
  return new Promise((resolve,reject)=>{
    let amount = employeeIncome.getAnnualIncome();
    let result;
    if(amount > 180000){
      result = BASE_FOR_180000 + aboveBaseTax((amount-180000),ABOVE_180000);
    }
    else if (amount > 80000){
      result = BASE_FOR_80000 + aboveBaseTax((amount-80000),ABOVE_80000);
    }
    else if (amount > 37000){
      result = BASE_FOR_37000 + aboveBaseTax((amount-37000),ABOVE_37000);
    }
    else if (amount > 18200){
      result = aboveBaseTax((amount-18200),ABOVE_18200);
    }
    else if(amount > 0){
      result = 0;
    }
    else {
      return new Error("Can't be a negative integer");
    }
    employeeIncome.setIncomeTax(roundOff(result/12));
    resolve(employeeInstance);
    })
}

//Multiplication of additional amount with cents/dollar
export function aboveBaseTax(amount,rate){
  return amount * rate;
}

//Promised based function diving yearly salary by 12
export function grossMonthlyIncomeFor(employeeInstance,employeeIncome){
  return new Promise((resolve,reject)=>{
    employeeIncome.setGrossIncome(roundOff(employeeIncome.getAnnualIncome()/12));
    resolve(employeeInstance);
  })
}

//Promise based function to exclude incomeTax from grossIncome
export function netIncome(employeeInstance,employeeIncome){
  return new Promise((resolve,reject)=>{
    employeeIncome.setNetIncome(employeeIncome.getGrossIncome() - employeeIncome.getIncomeTax());
    resolve(employeeInstance);
  })
}

//Promise based function to calculate monthly super based on evaluated grossIncome
export function totalSuperFor(employeeInstance,employeeIncome){
  return new Promise((resolve,reject)=>{
    employeeIncome.setSuperTax(roundOff(employeeIncome.getGrossIncome() * employeeIncome.getSuperAmount()));
    resolve(employeeInstance);
  })
}